/** Game of Life
		@author game by John H. Conway, implementation by Heiko Theißen */

/** Head of list of available cells.
		Alive cells are represented by instances of class LifeCell. For maximum
		object reuse, they are never discarded (and garbage-collected) but
		appended to a linked list of available cells with head availCell. */
var availCell;

/** Get a new cell instance.
		The instance taken from the list headed by availCell if it is not empty.
		Otherwise, a new instance is constructed. */
function getCell() {
	if (!availCell) return {};
	var r = availCell;
	availCell = availCell.next;
	return r;
}

/** Head of list of alive cells.
		Each LifeCell instance is either contained in the list headed by availCell
		or alive with certain x and y coordinates. The alive cells form a linked
		list that is sorted lexicographically by x and y. The head of this list 
		alive is an `empty' instance of LifeCell whose next link points to the first
		alive cell. A loop over all alive cells can be achieved by
		<pre>for ( var p = alive; p.next; p = p.next )</pre>
		where the body of the loop can evaluate p.next and a new cell r can be
		inserted before p.next by
		<pre>var r = p.next; p.next = getCell(); p.next.next = r;</pre>
		This list is also accessed by the UI control. */
var alive = getCell();

/** JSON object containing the state.
		This is also accessed by the UI control. */
var state;

/** Head of list of available counters.
		The factory for counters is analoguous to the cell factory.
		Counters are needed only temporarily when passing from one generation to
		the next, whereas alive cells are needed to store the overall state of
		the game. */
var availCounter;

/** Get a new counter instance */
function getCounter() {
	if (!availCounter) return {};
	var r = availCounter;
	availCounter = availCounter.next;
	return r;
}

/** Three pointers into the list of counters are needed.
		One at the beginning of x coordinate oldx - 1, one for x = oldx and one for
		x = oldx + 1. */
var count = [];
for (var i = 0; i <= 2; i++) count[i] = getCounter();

/** Earliest counter per x coordinate.
	  This is really a local variable for the generation method, but
		is declared outside to avoid repeated memory allocation. */
var t = [];

/** Linked list of lists of visible tiles. */
var tiles, Tiles;
var InvisibleTiles = {x: Infinity, X: Infinity};
InvisibleTiles.next = InvisibleTiles;

/** Zoom level of visible tiles. */
var tileZoom;

/** Whether the extreme coordinates shall be computed. */
var extremes;

/** Pass from one generation to the next.
		This method performs a loop over the alive cells and increases for
		each alive cell the counters of its 8 neighbours. At the end of each
		x coordinate, the counters for lesser x coordinates are complete,
		so they can be evaluated and cells are born, live on or die for these
		x coordinates. The corresponding counters are then freed again, so
		that only 3 lists of counters must be maintained at any time.
		This method is called from the UI control classes.
		@see #count */
window.onmessage = function() {

	/* Increase the generation counter. */
	state.gen++;
	
	/* Do nothing more if there are no alive cells. */
	if (alive.next) {

		/* After each animation frame, compute the extreme coordinates once. */
		if (extremes) {
			state.x = state.y = Infinity;
			state.X = state.Y = -Infinity;
		}

		var oldx = alive.next.x - 1;  // Old x coordinate
		var q = alive;  // First alive cell not yet updated
		var curtiles = tiles.next;

		/* Loop over the list of alive cells. */
		for (var c = alive.next; c; c = c.next) {
			
			/* After the current x coordinate has changed, update the cells with
				 x = oldx - 1, oldx or oldx + 1. */
			if (c.x !== oldx) {

				/* Keep track of extreme x coordinates. */
				if (extremes) {
					if (c.x < state.x) state.x = c.x;
					else if (c.x > state.X) state.X = c.x;
				}

				/* Find list of visible tiles for x = oldx - 1. */
				while (curtiles.X <= oldx - 1) curtiles = curtiles.next;

				q = updateX(q, oldx, c.x, curtiles);
				oldx = c.x;
				for (var i = 0; i <= 2; i++) t[i] = count[i];
			}

			/* Keep track of extreme y coordinates. */
			if (extremes) {
				if (c.y < state.y) state.y = c.y;
				else if (c.y > state.Y) state.Y = c.y;
			}

			/* Count the current alive cell as a neighbour for its 8 neighbours. */
			for (var x = c.x - 1, i = 0; i <= 2; x++ , i++) {
				var yncr = 1 + (i & 1);

				/* Start counter list traversal at t[i]. */
				var p = t[i];
				for (var y = c.y - 1, j = 0; j <= 2; y += yncr, j += yncr) {

					/* Traverse the counter list up to the given x and y coordinates. */
					while (p.next &&
						(p.next.x < x ||
							p.next.x === x && p.next.y < y)) p = p.next;

					/* Is there already a counter for x = c.x - 1 + i and y = j? */
					if (!p.next || p.next.x !== x || p.next.y !== y) {  // No.

						/* A new counter is needed only if it can still reach 2. Assumes B3S23 rule. */
						if ((i | j) !== 0) {

							/* Insert a new counter. */
							var r = p.next;
							p.next = getCounter();
							p.next.x = x;
							p.next.y = y;
							p.next.count = 1;
							p.next.next = r;

						}

						/* If a counter exists already, increase it. */
					} else p.next.count++;

					/* Advance t[i] once for each alive cell. */
					if (j === 0) t[i] = p;

				}
			}

		}

		/* After the last x coordinate has been completed, update the remaining cells. */
		while (curtiles.X <= oldx - 1) curtiles = curtiles.next;
		q = updateX(q, oldx, oldx + 3, curtiles);

		extremes = false;
	}

	/* Trigger the next generation. */
	postMessage(undefined, "*");

}

/** Update list of alive cells.
		This method is called after the counters for x coordinate oldx have been
		completed. It lets cells with x coordinates oldx - 1, oldx and oldx + 1
		(but not newx - 1 or higher) be born or die according to their
		number of neighbours.
		@param q pointer into list of alive cells.
			q points before the first alive cell with x &ge; oldx - 1 (there need not
			be any alive cells with x = oldx - 1).
			In this method, q is advanced over cells that will be alive in the next
			generation, so at any time only cells after q must be checked by this
			method.
		@param oldx x coordinate whose counters have been completed
		@param newx x coordinate whose counters have not yet been completed
		@param curtiles pointer to list of visible tiles for x = oldx - 1
		@return updated value of q */
function updateX(q, oldx, newx, curtiles) {
	var curt;
	
	/* Loop over the three x coordinates before, on and after oldx, but
		 do not exceed newx - 2. */
	for (var i = 0; i <= 2 && oldx + i < newx; i++) {
		var curx = oldx - 1 + i;
		
		/* Let curt be the list of visible tiles for curx, at either curtiles or curtiles.next. */
		if (curtiles.x <= curx && curtiles.X > curx) curt = curtiles.up;
		else if (curtiles.next.x <= curx && curtiles.next.X > curx) curt = curtiles.next.up;
		else curt = null;

		/* Loop over the counters for x = oldx - 1 + i. */
		var s = count[i];  // lags one step behind p
		for (var p = count[i].next; p && p.x === curx; p = p.next) {

			/* Advance q.next to the cell which p counts or to the position
				 where such a cell must be inserted.				
				 The passed-over cells have no counter and will die. */
			var r = q;
			while (r.next &&
				(r.next.x < p.x ||
					r.next.x === p.x && r.next.y < p.y)) {
				r = r.next;
				curt = updateCanvas(curt, r, -1);
			}
			if (r !== q) {
				var r1 = q.next;
				q.next = r.next;
				/* Free the list of died cells (which runs from r1 to r). */
				r.next = availCell;
				availCell = r1;
			}

			/* Was the cell q.next alive in the previous generation? */
			if (q.next && q.next.x === p.x && q.next.y === p.y) {  // Yes

				/* Will it die in the next generation (which happens if its counter
					 is neither 10 nor 11 in binary)? */
				if (p.count >>> 1 !== 1) {  // Yes. Assumes B3S23 rule.
					curt = updateCanvas(curt, q.next, -1);
					died(q);
				}

				/* The cell remains alive in the next generation, pass over it. */
				else q = q.next;

				/* The cell was not alive, will one be born in the next generation? */
			} else if (p.count === 3) {  // Yes. Assumes B3S23 rule.
				curt = updateCanvas(curt, p, 1);
				born(q, p.x, p.y);
				q = q.next;  // Pass over the new cell.
			}

			s = p;
		}

		/* Free the list of counters for x = oldx - 1 + i (which runs
			 from count[i].next to s). */
		s.next = availCounter;
		availCounter = count[i].next;

	}

	/* Renumber the counters whose x coordinate exceeds newx - 1 such that
		 count[i] heads the list of counters for x coordinate newx - 1 + i. */
	for (i = 0; i <= 2; i++)
		if (oldx + 2 >= newx + i)
			count[i].next = count[i + newx - oldx].next;
		else
			count[i].next = null;

	return q;
}

/** Set or reset a cell on the canvas.
		Also increases or decreases the population counter.
		@param curt pointer into list of visible tiles for current x coordinate
		@param r node containing x and y coordinates of current cell
		@param sign +1 to set the cell, -1 to reset it
		@return updated value of curt */
function updateCanvas(curt, r, sign) {
	state.pop += sign;
	if (curt) {
		while (curt.Y <= r.y) curt = curt.up;
		if (curt.y <= r.y) curt.setCanvas(r.x, r.y, sign);
		else if (curt.y === Infinity) curt = null;
	}
	return curt;
}

/** Insert a new cell with given coordinates before q.next. */
function born(q, x, y) {
	var r = q.next;
	q.next = getCell();
	q.next.x = x;
	q.next.y = y;
	q.next.next = r;
}

/** Remove the cell q.next from the live of alive cells.
		Also return it to the list of available cells. */
function died(q) {
	var r = q.next.next;
	q.next.next = availCell;
	availCell = q.next;
	q.next = r;
}

/** Canvas layer for Leaflet. */
const CanvasLayer = L.GridLayer.extend({
	initialize: function(options) {
		L.GridLayer.prototype.initialize.apply(this, arguments);
		this.tileSize = options.tileSize;
	},
	createTile: function(coords) {
		return new lifeTile(this, coords).tile;
	},
	toggleClear: function() {
		this.clearPixel = !this.clearPixel;
		this.redraw();
	}
});

/** Leaflet tile containing a drawing context.
		@param layer grid layer to which the tile belongs
		@param coords coordinates of this tile as passed to the createTile method
		@see CanvasLayer */
function lifeTile(layer, coords) {
	this.tile = L.DomUtil.create("canvas");
	this.tile.lifeTile = this;
	this.tile.width = layer.tileSize.x;
	this.tile.height = layer.tileSize.y;
	var s = 1 << -coords.z;  // canvas scale-out factor
	var mask = ~(s - 1);  // mask for deleting pixels
	var x = coords.x * layer.tileSize.x * s;
	var X = (coords.x + 1) * layer.tileSize.x * s;
	
	/* Expose y coordinates, because they are evaluated by updateCanvas. */
	this.y = coords.y * layer.tileSize.y * s;
	this.Y = (coords.y + 1) * layer.tileSize.y * s;

	/* Insert new tile into list of lists. */
	if (coords.z !== tileZoom) {
		tiles = Tiles = {x: Infinity, X: Infinity};
		tiles.next = tiles.prev = tiles;
		tileZoom = coords.z;
	}
	var r = tiles;
	while (r.next.x < x) r = r.next;
	if (r.next.x > x) {
		var n = r.next;
		r.next = {x: x, X: X, y: Infinity, Y: Infinity, next: n, prev: r};
		r.next.up = r.next.down = r.next;
		n.prev = r.next; 
	}
	r = r.next;
	while (r.up.y < this.y) r = r.up;
	this.up = r.up;
	this.down = r;
	this.up.down = this;
	r.up = this;

	var ctx = this.tile.getContext("2d");
	ctx.scale(1 / s, 1 / s);
	ctx.translate(-x, -this.y);

	/* Representation of the canvas where Life on this tile plays out. */
	var arrayLength = 1024;
	var messageSign = new Int8Array(arrayLength);
	var messageX = new Array(arrayLength);
	var messageY = new Array(arrayLength);
	/* Expose messageLength, because it is cleared by the redraw method. */
	this.messageLength = 0;

	/** Record a life event for a pixel.
			Must be called only if the pixel belongs to this tile. */
	this.setCanvas = function(x, y, sign) {
		if (this.messageLength === arrayLength) {
			messageX = messageX.concat(new Array(arrayLength));
			messageY = messageY.concat(new Array(arrayLength));
			arrayLength *= 2;
			var tmp = messageSign;
			messageSign = new Int8Array(arrayLength);
			messageSign.set(tmp);
		}
		messageSign[this.messageLength] = sign;
		messageX[this.messageLength] = x;
		messageY[this.messageLength] = y;
		this.messageLength++;
	};
	
	/** Update rendering of this tile to reflect recent live events. */
	this.render = function() {
		for (var i = 0; i < this.messageLength; i++) {
			if (messageSign[i] === 1)
				ctx.fillRect(messageX[i], messageY[i], 1, 1);
			else if (s === 1 || !layer.clearPixel)
				ctx.clearRect(messageX[i], messageY[i], 1, 1);
			else
				/* When s > 1, clear the whole pixel when a subpixel is reset. */
				ctx.clearRect(messageX[i] & mask, messageY[i] & mask, s, s);
		}
		this.messageLength = 0;
	};

	/** Redraw one pixel.
			Must be called only if the pixel belongs to this tile. */
	this.redraw = function(x, y) {
		ctx.fillRect(x, y, 1, 1);
	};
}

/** Render all visible tiles. */
function render() {
	for (var r = tiles.next; r !== tiles; r = r.next)
		for (var t = r.up; t !== r; t = t.up) t.render();
	document.getElementById("state").textContent = JSON.stringify(state);
	extremes = true;
	requestAnimationFrame(render);
}

var layer;

document.addEventListener("visibilitychange", function() {
  if (document.visibilityState === "visible") {
  	tiles = Tiles;
  	if (layer) layer.redraw();
  } else
  	tiles = InvisibleTiles;
});

function createMap(canvasId, sizeX, sizeY) {
	var map = L.map(canvasId, {
		crs : L.CRS.Simple
	}).setView([0, 0], 0);
	var b = map.getPixelBounds();
	var dx = b.max.x - b.min.x;
	var dy = b.max.y - b.min.y;
	var z = 0;
	while (sizeX > dx || sizeY > dy) {
		z--;
		dx *= 2;
		dy *= 2;
	}
	layer = new CanvasLayer({
		tileSize : L.point(Math.floor((b.max.x - b.min.x) / 2), Math.floor((b.max.y - b.min.y) / 2)),
		minZoom : -Infinity,
		maxZoom : 0
	}).on("tileunload", function(event) {
		var t = event.tile.lifeTile;
		if (t.up) {
			t.down.up = t.up;
			t.up.down = t.down;
			if (t.up.next) {
				t.up.prev.next = t.up.next;
				t.up.next.prev = t.up.prev;
			}
		}
	}).on("load", function(event) {
		/* Redraw all visible tiles from scratch */
		var curtiles = tiles.next;
		var curt;
		var oldx;
		for (var c = alive.next; c; c = c.next) {
			if (c.x !== oldx) {
				while (curtiles.X <= c.x) curtiles = curtiles.next;
				if (curtiles.x <= c.x && curtiles.X > c.x) {
					curt = curtiles.up;
					curt.messageLength = 0;
				}
				else curt = null;
				oldx = c.x;
			}
			if (curt) {
				while (curt.Y <= c.y) {
					curt = curt.up;
					curt.messageLength = 0;
				}
				if (curt.y <= c.y) curt.redraw(c.x, c.y);
				else if (curt.y === Infinity) curt = null;
			}
		}
	}).addTo(map);
	map.setZoom(z);
}

function loadLIF() {
	var code = document.getElementById("form").lif.value;
	code = code.split("\n");
	while (code[0].charAt(0) == "#") code.shift();
	var size = code.shift().split(/\s*[,=]\s/);
	state = {gen: 0, pop: 0, x: Infinity, X: -Infinity, y: Infinity, Y: -Infinity};
	code = code.join("").split("!")[0].split("$");
	var message = [];
	for (var y = 0, i = 0; i < code.length; y++ , i++) {
		var regex = /(\d*)([bo])/g;
		var m;
		var x = 0;
		while (m = regex.exec(code[i])) {
			var c = Number(m[1]) || 1;
			if (m[2] == "o") {
				for (var j = 0; j < c; j++) {
					message.push({x: x, y: y});
					x++;
				}
				state.pop += c;
				if (x < state.x) state.x = x;
				else if (x > state.X) state.X = x;
				if (y < state.y) state.y = y;
				else if (y > state.Y) state.Y = y;
			} else x += c;
		}
		if (m = /\d+$/.exec(code[i]))
			y += Number(m[0]) - 1;
	}
	/* Recycle all alive cells. */
	c = alive;
	while (c.next) c = c.next;
	if (c !== alive) {
		c.next = availCell;
		availCell = alive.next;
		alive.next = null;
	}
	message.sort(function(a, b) {
		if (a.x < b.x) return -1;
		if (a.x > b.x) return 1;
		if (a.y < b.y) return -1;
		if (a.y > b.y) return 1;
		return 0;
	});
	for (var i = 0, q = alive; i < message.length; i++ , q = q.next)
		born(q, message[i].x, message[i].y);
		
	createMap("canvas", size[1], size[3]);
	document.getElementById("state").textContent = JSON.stringify(state);
}

function load() {
	var form = document.getElementById("form");
	fetch(form.url.value).then(_ => _.text()).then(function(p) {
		form.lif.value = p;
	});
}

function display() {
	document.body.classList.add("running");
	loadLIF();
}

function run() {
	postMessage(undefined, "*");
	requestAnimationFrame(render);
}
