const express = require("express");
const https = require("https");

module.exports = express()
	.use("/code/Patterns", function(req, res, next) {
		https.get(`https://sourceforge.net/p/golly/code/ci/master/tree/Patterns${req.url}?format=raw`)
			.on("response", function(r) {
				res.writeHead(r.statusCode, r.statusMessage, r.headers);
				r.pipe(res);
			})
			.on("error", next);
	})
	.use(express.static(__dirname));
